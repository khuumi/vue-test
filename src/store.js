import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      firstName: '',
      lastName: ''
    }
  },
  getters: {
    user: state => state.user
  },
  mutations: {
    UPDATE_USER (state, user) {
      state.user = user
    }
  },
  actions: {
    update_user ({commit}, user) {
      commit('UPDATE_USER', user)
    }
  }
})
