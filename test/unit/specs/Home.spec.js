import Home from '@/components/Home'
import { shallowMount } from '@vue/test-utils'

describe('Home.vue', () => {
  it('should render correct first name', () => {
    const wrapper = shallowMount(Home)
    const firstName = 'testing'

    wrapper.setData({user: {firstName: firstName}})

    expect(wrapper.find('div#firstName').text())
      .to.equal(firstName)
  })
})
